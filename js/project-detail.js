$(document).ready(function () {
    if ($('.project-detail-slider').length > 0) {
        $(".project-detail-slider").each(function () {
            $(this).owlCarousel({
                loop: true,
                lazyLoad: true,
                margin: 30,
                responsiveClass: true,
                items: 1,
                dots: false,
                navText: ["<i class='icon-arrow-left-slider'></i>", "<i class='icon-arrow-left-slider'></i>"],
                rewindNav: true,
                nav: true,
            })
        })
    }
});