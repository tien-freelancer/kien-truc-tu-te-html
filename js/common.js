$(document).ready(function () {
    function closeMenu() {
        $('.t-mobile-menu').removeClass('show');
        $('.overlay-menu').removeClass('active');
    }
    $(".has-submenu > .btn-toggle-sub").on("click", function (e) {
        var parentli = $(this).closest('li');
        if (parentli.hasClass('opened')) {
            parentli.removeClass('opened');
            parentli.find('> ul.sub-menu').slideUp(400);
        } else {
            parentli.addClass('opened');
            parentli.find('> ul.sub-menu').slideDown(400);
        }
        parentli.siblings('li').removeClass('opened');
        parentli.siblings('li').find('.has-submenu.opened').removeClass('opened');
        parentli.siblings('li').find('ul:visible').slideUp();
    })
    $('.btn-menu-mobile').on("click", function () {
        $('.overlay-menu').toggleClass("active");
        $(".t-mobile-menu").toggleClass("show");
        return false;
    })
    $('.overlay-menu, .m-menu-close').on("click", function () {
        closeMenu();
    })
});


$(document).ready(function () {
    //Loop animation
    // $.doTimeout(2500, function () {
    //     $('.repeat.go').removeClass('go');
    //     return true;
    // });
    // $.doTimeout(2520, function () {
    //     $('.repeat').addClass('go');
    //     return true;
    // });

    $('.search-header .close').click(function () {
        $('.search-header').slideToggle();
    })

    $('#btn-search').click(function (e) {
        $('.search-header').slideToggle();
    });
});