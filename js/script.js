$(document).ready(function () {
    if ($('.js-home-slider').length > 0) {
        $('.js-home-slider').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 4000,
            autoplaySpeed: 1000,
            // nestedItemSelector: 'item-gall',
            lazyLoad: true,
            margin: 0,
            responsiveClass: true,
            // dotsContainer: '#home-custom-dots',
            items: 1,
            dots: true,
            navText: ["<i class='icon-arrow-left-slider'></i>", "<i class='icon-arrow-left-slider'></i>"],
            rewindNav: true,
            nav: true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1,
                    nav: false,
                },
                576: {
                    items: 1,
                    nav: false,
                },
                768: {
                    items: 1,
                    nav: false,
                },
                992: {
                    items: 1,
                    nav: true,
                },
                1200: {
                    items: 1,
                }
            }
        })
        // $('.box-dot-slider').on('click', '.owl-dot', function (e) {
        //     owl.trigger('to.owl.carousel', [$(this).index(), 300]);
        //     console.log(aâ);
        // });
    }

    if ($('.js-project-slider').length > 0) {
        $('.js-project-slider').owlCarousel({
            loop: true,
            lazyLoad: true,
            margin: 30,
            responsiveClass: true,
            items: 2,

            dots: false,
            navText: ["<i class='icon-arrow-left-slider'></i>", "<i class='icon-arrow-left-slider'></i>"],
            rewindNav: true,
            nav: true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1, dots: true, nav: false,
                },
                576: {
                    items: 1, dots: true, nav: false,
                },
                768: {
                    items: 2, dots: true, nav: false,
                },
                992: {
                    items: 2,
                },
                1200: {
                    items: 2,
                    stagePadding: 100,
                }
            }
        })

    }

    if ($('.js-blog-slider').length > 0) {
        $('.js-blog-slider').owlCarousel({
            loop: true,
            lazyLoad: true,
            margin: 30,
            responsiveClass: true,
            items: 3,
            dots: false,
            navText: ["<i class='icon-arrow-left-slider'></i>", "<i class='icon-arrow-left-slider'></i>"],
            rewindNav: true,
            nav: true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1, dots: true, nav: false,
                },
                576: {
                    items: 2, dots: true, nav: false,
                },
                768: {
                    items: 2, dots: true, nav: false,
                },
                992: {
                    items: 3,
                },
                1200: {
                    items: 3,
                }
            }
        })

    }
});