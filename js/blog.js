$(document).ready(function () {
    if ($('.js-blog-slider').length > 0) {
        $('.js-blog-slider').owlCarousel({
            loop: true,
            lazyLoad: true,
            margin: 30,
            responsiveClass: true,
            items: 3,
            dots: true,
            navText: ["<i class='icon-arrow-left-slider'></i>", "<i class='icon-arrow-left-slider'></i>"],
            rewindNav: true,
            nav: true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1, dots: true, nav: false,
                },
                576: {
                    items: 2, dots: true, nav: false,
                },
                768: {
                    items: 2, dots: true, nav: false,
                },
                992: {
                    items: 3,
                },
                1200: {
                    items: 3,
                }
            }
        })

    }
});