$(document).ready(function () {

    // FAQ JS
    $(".faq-item .faq-title").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active").next(".faq-content").slideUp();
        } else {
            $(".faq-item .faq-title").removeClass("active")
            $(".faq-item .faq-content").slideUp();
            $(this).toggleClass("active").next(".faq-content").slideToggle();
        }
    })
    $(".faq-section .faq-item .faq-title").first().trigger('click');

});