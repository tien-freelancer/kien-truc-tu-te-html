$(document).ready(function () {

    if ($('.js-project-slider').length > 0) {
        $('.js-project-slider').owlCarousel({
            loop: false,
            lazyLoad: true,
            margin: 30,
            responsiveClass: true,
            items: 2,

            dots: true,
            navText: ["<i class='icon-arrow-left-slider'></i>", "<i class='icon-arrow-left-slider'></i>"],
            rewindNav: true,
            nav: true,
            responsive: {
                // breakpoint from 0 up
                0: {
                    items: 1, dots: true, nav: false,
                },
                576: {
                    items: 1, dots: true, nav: false,
                },
                768: {
                    items: 2, dots: true, nav: false,
                },
                992: {
                    items: 2,
                },
                1200: {
                    items: 2,
                    stagePadding: 100,
                }
            }
        })

    }
});